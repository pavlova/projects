<div class="container-fluid container1">
    <div class="nav-row  navbar-fixed-top">
        <div class="menu">
            <a type="button" class="btn-default col-md-1 col-xs-3 btn-menu text-left" data-toggle="modal"
                data-target="#myModal"><i class="fa fa-bars"></i> Menu</a>
        </div>
        <div class="text-center col-md-2 col-xs-6 col-md-offset-4">
            <a href="#"><img src="images/logo.png" alt="" class="logo"></a>
        </div>
        <div class="nav-btn-right col-md-5 hidden-xs hidden-sm text-right">
            <a href="https://www.brainster.io/business" class="btn1-nav btn btn-default">Обуки за компании</a>
            <a href="https://www.brainster.io/business" class="btn2-nav btn btn-default">Вработи наши студенти</a>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div class="logo-modal">
                    <a href="https://brainster.co/"><img src="images/logo.png" alt="" class="logo-modal-1"></a>
                </div>
                <div class="button-close">
                    <button type="button" class="close " data-dismiss="modal"><span aria-hidden="true"
                            class="close1">&times;</span><span class="close2">Затвори</span></button>
                </div>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <li class="list-group-item"><a href="form/login.php?action=login" class="y">Најави се</a>
                    </li>
                    <li class="list-group-item"><a href="form/login.php?action=register" class="y">Регистрирај
                            се</a></li>
                    <li class="list-group-item"><a href="About_Us.php" class="y">За
                            нас</a></li>
                    <li class="list-group-item"><a href="https://www.facebook.com/pg/brainster.co/photos/"
                            class="y">Галерија</a>
                    </li>
                    <li class="list-group-item"><a href="Contact.php" class="y">Контакт</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>