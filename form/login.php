<?php
$title = 'LoginOrRegister';
include 'head1.php';
?>

<body class="lg_rg">
    <?php
        if(isset($_GET["action"]) && $_GET["action"] == "login"){
    ?>
    <div class="container">
        <div class="row ">
        <div class="col-md-2 col-xs-12">
                <a href="../proekt1_1.php"><img src="logo.png" alt="" class="logo_result"></a>
            </div>
            <div class="col-md-12 login_or_register">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2 class="log">Најави се</h2>
                    <form method="POST" action="LoginOrRegister.php?login" class="login">
                        <label for="email">Email:</label> 
                        <br>
                        <input type="email" name="email"/>
                        <br>
                        <br>
                        <label for="password">Password:</label> 
                        <br>
                        <input type="password" name="password"/>
                        <br>
                        <?php if (isset($_GET['error']) && $_GET['error']=='invalid_email_or_password'){
                            echo "<br>*Invalid email or password" ;
                        } ?>
                        <br>
                        <br>
                        <button type="submit" class="submit btn-primary submit1 btn1-nav">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php

    }else{   

    ?>
    <div class="container">
        <div class="row ">
        <div class="col-md-2 col-xs-12 ">
                <a href="../proekt1_1.php"><img src="logo.png" alt="" class="logo"></a>
            </div>
            <div class="col-md-12 login_or_register">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2 class="reg">Регистрирај се</h2>
                    <form method="POST" action="LoginOrRegister.php?register" class="register">
                        <input type="text" name="firstName" placeholder="First Name" required = 'required'/>
                        <br>
                        <br>
                        <input type="text" name="lastName" placeholder="Last Name" required = 'required'/>
                        <br>
                        <br>
                        <input type="text" name="company" placeholder="Company" required = 'required'/>
                        <?php if (isset($_GET['error']) && $_GET['error']=='duplicate_company'){
                            echo "*Duplicate company" ;
                        }?>
                        <br>
                        <br>
                        <input type="email" name="email" placeholder="Email" required = 'required'/>
                        <?php if (isset($_GET['error']) && $_GET['error']=='invalid_email'){
                            echo "*Invalid email" ;
                        }?>
                        <br>
                        <br>
                        <input type="password" name="password" placeholder="Password" required = 'required'/>
                        <?php if (isset($_GET['error']) && $_GET['error']=='invalid_password'){
                            echo "*The password is to short" ;
                        }?>
                        <br>
                        <br>
                        <input type="tel" name="phoneNumber" placeholder="Phone Number (Optional)"/>
                        <br>
                        <br>
                        <label for="number_of_employees">Number of employees:</label>
                        <select class="form-control"  name="number_of_employees" >
                            <option>1</option>
                            <option>2-10</option>
                            <option>11-50</option>
                            <option>51-200</option>
                            <option>200+</option>
                        </select>
                        <br>
                        <label for="department">Department</label>
                        <select class="form-control" name="department" >
                            <option>Human Resources</option>
                            <option>Marketing</option>
                            <option>Product</option>
                            <option>Sales</option>
                            <option>SEO</option>
                        </select>
                        <br>
                        <textarea class="form-control" rows="5" name="comments" placeholder="Please use this space to tell us more about your need. Our team can craft custom solutions around reskilling, upskilling, onboarding, hiring, and benchmarking your talent."></textarea>
                        <br>
                        <br>
                        <button type="submit" class="submit btn-primary submit1 btn1-nav">Register</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
        }
    ?>

</body>
</html>


