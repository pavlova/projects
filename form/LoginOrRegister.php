<?php
if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $firstname = $_POST['firstName'];
    $lastname = $_POST['lastName'];
    $company = $_POST['company'];
    $email = $_POST['email'];
    $phoneNum = $_POST['phoneNumber'];
    $number_of_employees = $_POST['number_of_employees'];
    $department = $_POST['department'];
    $comments = $_POST['comments'];
    $password = $_POST['password'];

    if(strlen($email) == 0 || strlen($password) == 0 ) {
        header("Location: result.php?error=required");
        die();
    }  

    if(isset($_GET['login'])){
        login( $email, $password);
    } else{
        if(strlen($firstname) == 0 || strlen($lastname) == 0 || strlen($company) == 0 || strlen($email) == 0 || strlen($password) == 0){
            header("Location: login.php?error=required");
            die();
        }
        $validEmail = isValidEmail($email);
        if($validEmail == false){
            header("Location: login.php?error=invalid_email"); 
            die();
        }
        $validPassword = isValidPassword($password);
        if($validPassword == false){
            header("Location: login.php?error=invalid_password"); 
            die();
        }
        
        register($firstname, $lastname, $company, $email, $phoneNum, $number_of_employees, $department, $comments, $password);
    }

} else {
    header("Location: login_register.php");
    die();
}


function isValidPassword($password){
    if(strlen($password)>=6){
        return true;
    } else{
        header("Location: login.php?error=invalid_password"); 
    }
}

function isValidEmail($email){
    $a = strpos($email, "@");
    if ($a >= 5){
        return true;
    } else{
        header("Location: login.php?error=invalid_email"); 
    }
}

function login($email, $password){

    $myPDO = new PDO("mysql:host=localhost;dbname=project_1", 'root', '6593');
    $myPDO->exec("SET CHARACTER SET utf8");

    $x = $myPDO->query("
        SELECT * FROM `users` WHERE `email` = '$email'
    ");

    $users = $x->fetchAll();
    if(count($users)==1){
        $pwd = $users[0]['password'];
        if(md5($password) == $pwd){
            header("Location: result.php?success=login&name=$email");
            die();
        }
}
    header("Location: login.php?action=login&error=invalid_email_or_password");
    die();

}


function register($firstname, $lastname, $company, $email, $phoneNum, $number_of_employees, $department, $comments, $password){
    $myPDO = new PDO("mysql:host=localhost;dbname=project_1", 'root', '6593');
    $myPDO->exec("SET CHARACTER SET utf8");

    $x = $myPDO->query("
        SELECT * FROM `users` WHERE `email` = '$email'
    ");

    $users = $x->fetchAll();

    if(count($users)>0){
        header("Location: login.php?error=duplicate_email");
        die();
    }

    $x = $myPDO->prepare("INSERT 
        INTO `users` (`first_name`, `last_name`, `company`, `email`, `phone_number`, `number_of_employees`, `department`, `comments`, `password`)
        VALUES (:fn, :ln, :company, :email, :num, :emp, :depart, :com, :pass);
    ");

    $x->bindParam(':fn', $firstname);
    $x->bindParam(':ln', $lastname);
    $x->bindParam(':company', $company);
    $x->bindParam(':email', $email);
    $x->bindParam(':num', $phoneNum);
    $x->bindParam(':emp', $number_of_employees);
    $x->bindParam(':depart', $department);
    $x->bindParam(':com',  $comments);
    $x->bindParam(':pass',  md5($password));


    $y = $x->execute();
    if($y == true){
        header("Location: result.php?success=register&name=$company");
    }else{
        header("Location: result.php?error");
    }
    die();
}

?>




