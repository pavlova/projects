<div class="container-fluid footer-about">
    <div class="footer ">
        <div class="container col-md-12 col-xs-12">
            <div class=" text-right visible-xs col-xs-12 text-center row3-3 ">
                <a href="https://www.linkedin.com/school/brainster-co/"><i class="fab fa-linkedin-in"></i></a>
                <a href="https://twitter.com/brainsterco"><i class="fab fa-twitter"></i></a>
                <a href="https://www.facebook.com/brainster.co/"><i class="fab fa-facebook-f"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-left col-md-8 col-sm-8 col-xs-12 row3-1 row3-1-about">
                <li><a href="https://brainster.co/about">За нас</a></li>
                <li><a href="https://brainster.co/contact">Контакт</a>
                </li>
                <li><a href="https://blog.brainster.co/">Блог</a>
                </li>
                <li><a href="https://brainster.co/courses#!/grid/infinite/throttle:30/threshold:30/">Курсеви</a>
                </li>
                <li><a href="https://brainster.co/faq">ЧПП</a>
                </li>
                <li><a href="https://blog.brainster.co/brainster-во-медиумите/">Во медиумите</a>
                </li>
            </ul>
            <div class=" text-right col-md-4 col-sm-4 hidden-xs row3-3 ">
                <a href="https://www.linkedin.com/school/brainster-co/"><i class="fab fa-linkedin-in"></i></a>
                <a href="https://twitter.com/brainsterco"><i class="fab fa-twitter"></i></a>
                <a href="https://www.facebook.com/brainster.co/"><i class="fab fa-facebook-f"></i></a>
            </div>
        </div>
    </div>
</div>