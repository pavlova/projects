<div class="container-fluid container4">
    <div class="row1 row4">
        <h1>Future-proof your organization</h1>
        <div class="col-md-6 col-md-offset-3 text-cener">
            <p class="p2">Find out how to unlock
                progress in your business.
                Understand your current state,
                indentify
                opportunity areas and preparate to
                take charge of your organization's
                future.</p>
            <div class="text-center"><a href="https://brainsterquiz.typeform.com/to/kC2I9E" target="blank"
                    class="btn btn-default btn1-nav btn4 ">Take
                    the assessment</a>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class=" row3">
        <nav class="navbar navbar-default navbar-default-3">
            <div class="container col-md-10 col-xs-12 col-md-offset-1">
                <div class=" text-right visible-xs col-xs-12 text-center row3-3 ">
                    <a href="https://www.linkedin.com/school/brainster-co/"><i class="fab fa-linkedin-in"></i></a>
                    <a href="https://twitter.com/brainsterco"><i class="fab fa-twitter"></i></a>
                    <a href="https://www.facebook.com/brainster.co/"><i class="fab fa-facebook-f"></i></a>
                </div>
                <ul class="nav navbar-nav navbar-left col-md-4 col-sm-4 col-xs-12 row3-1">
                    <li><a href="About_Us.php">About
                            us</a></li>
                    <li><a href="Contact.php">Contact</a>
                    </li>
                    <li><a href="https://www.facebook.com/pg/brainster.co/photos/">Gallery</a>
                    </li>
                </ul>
                <div class="col-md-4 col-sm-4 col-xs-12 text-center row3-2">
                    <a href="#"><img src="images/logo.png" alt="" class="logo"></a>
                </div>
                <div class=" text-right col-md-4 col-sm-4 hidden-xs row3-3 ">
                    <a href="https://www.linkedin.com/school/brainster-co/"><i class="fab fa-linkedin-in"></i></a>
                    <a href="https://twitter.com/brainsterco"><i class="fab fa-twitter"></i></a>
                    <a href="https://www.facebook.com/brainster.co/"><i class="fab fa-facebook-f"></i></a>
                </div>
            </div>
        </nav>
    </div>
</div>