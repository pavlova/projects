<?php
$title = 'About Us';
include 'head.php';
?>
<?php include 'header1.php';?>
<div class="container-fluid">
    <div class="about">
        <h1>За нас</h1>
    </div>
</div>    

<div class="image-fix">
    <h2 class="learn">Учи, сподели, вмрежи се!</h2>
</div>

<div class="container">
    <div class="col-md-10 col-md-offset-1 text-center about-text">
        <h2 >Ајде заедно да го промениме начинот на кој учиме!</h2>
        <p>Ние веруваме дека секој поседува практични вештини да предава како и постојана потреба да стекнува нови знаења.</p>
        <p>Затоа гo создадовме Brainster.</p>
        <p>Brainster е платформа за офлајн курсеви каде ќе можете да предавате и да посетувате курсеви на најразлични теми - од курсеви за изработка на мобилни апликации, до курсеви за улична фотографија.</p>
        <h2>Нашата визија е да го претвориме целиот град во универзитет, секој простор во училница и секој од нас во инструктор и студент.</h2>
        <p>Brainster Ви овозможува активно да учествувате во градењето на локалната 2.0 заедница, да се вмрежите со луѓе со слични интереси, да креирате, да го споделувате Вашето знаење и да учите од другите.</p>
        <p>Нашата прва станица е Скопје. Очекувајте нѐ во октомври на различни кул локации како co-working простории, работилници, акселератори, па дури и во кафулиња.</p>
        <h2>Контакт</h2>
        <p>contact@brainster.co</p>
    </div>
</div>
    
<?php include 'footer1.php';?>

        



    











    






    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

    <script>
        $(document).ready(function () {
            $(window).scroll(function () {
                var scroll = $(window).scrollTop();
                if (scroll > 10) {
                    $(".nav-row").css("background", "#333");
                }

                else {
                    $(".nav-row").css("background", "#5f5f5f");
                }
            })
        })

    </script>
</body>

</html>