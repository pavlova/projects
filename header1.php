<body class="About_Us">
    <div class="container-fluid container1">
        <div class="nav-row  navbar-fixed-top nav-about">
            <div class="menu col-md-2 col-xs-12 col-sm-4 col-xs-12">
                <a href="proekt1_1.php"><img src="images/logo.png" alt="" class="logo"></a>  
            </div>
            <div class="nav-btn-right col-md-8 col-sm-4 col-xs-12 text-center">
                <a href="form/login.php?action=login" class="btn1-nav btn btn-default">Најави се</a>
                <a href="form/login.php?action=register" class="btn1-nav btn btn-default">Регистрирај се</a>
            </div>
            <div class="nav-btn-right text-right col-md-2 col-sm-4 hidden-xs">
                <a href="http://codepreneurs.co/" class="btn1-nav btn btn-default">Академии</a>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

    <script>
        $(document).ready(function () {
            $(window).scroll(function () {
                var scroll = $(window).scrollTop();
                if (scroll > 10) {
                    $(".nav-row").css("background", "#333");
                }

                else {
                    $(".nav-row").css("background", "#5f5f5f");
                }
            })
        })

    </script>
</body>

</html>