<?php
$myPDO = new PDO("mysql:host=localhost;dbname=project_1", 'root', '6593');
$myPDO->exec("SET CHARACTER SET utf8");

$x = $myPDO->query("
    SELECT * FROM games;
");

$games = $x->fetchAll();
$title = 'Homepage';
include 'head.php';
?>

<body>
    <?php include 'header.php';?>
    <div class="container-fluid container2">
        <div class="row1 col-md-12">
            <p class="p1">Изработено од студенти на академијата за програмирање на <a href="https://brainster.co/"
                    class="p1-1">Brainster</a></p>
            <div class="nav-btn-right col-xs-12 hidden-lg hidden-md text-center">
                <a href="https://www.brainster.io/business" class="btn1-nav btn btn-default">Обуки за компании</a>
                <a href="https://www.brainster.io/business" class="btn2-nav btn btn-default">Вработи наши студенти</a>
            </div>
            <h1>Future-proof your organization</h1>
            <div class="col-md-6 col-md-offset-3 text-cener">
                <p class="p2">Find out how to unlock progress in your business. Understand your current state, indentify
                    opportunity areas and preparate to take charge of your organization's future.</p>
                <div class="text-center"><a href="https://brainsterquiz.typeform.com/to/kC2I9E" target="blank"
                        class="btn btn-default btn1-nav btn4 ">Take
                        the assessment</a>
                </div>
            </div>
        </div>
    </div>
    <div class="background"></div>
    <div class="container-fluid">
        <div class="row2 col-lg-10 col-md-12 col-sm-12 col-lg-offset-1">
            <?php foreach ($games as $key => $value) { ?>
            <div class="col-md-4 col-sm-6 row2-1">
                <a href="single_page.php?game=<?php echo $value['id']; ?>" target="_blank">
                    <div class="panel-default panel-default-1">

                        <div class="panel-body panel-body-1">
                            <div class="game">
                                <img src="<?php echo $value['image']; ?>" alt=""
                                    class="hidden-xs hidden-xs img-responsive image">
                            </div>
                        </div>
                        <div class="panel-body panel-body1-1">
                            <div class="panel-body-2 col-xs-10">
                                <h5><?php echo $value['name']; ?></h5>
                                <p>Категорија: <span class="category-span"><?php echo $value['category']; ?></span> </p>
                            </div>
                            <div class="panel-body-3 col-xs-2">
                                <img src="<?php echo $value['image']; ?>" alt="">
                            </div>
                            <div class="panel-body-4 col-xs-12">
                                <i class="far fa-clock fa-clock-1 col-xs-1"></i>
                                <h5 class="time col-xs-6">Времетраење
                                    <span><br><?php echo $value['time_frame']; ?></span> </h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php   } ?>
        </div>
    </div>
    <div class="background-footer"></div>
    <?php include 'footer.php'; ?>
    <?php include 'scripts.php';?>
</body>
</html>