<?php
$game_id = $_GET['game']; 

$myPDO = new PDO("mysql:host=localhost;dbname=project_1", 'root', '6593');
$myPDO->exec("SET CHARACTER SET utf8");

$x = $myPDO->query("
    SELECT * FROM `games` WHERE id = $game_id;
");
$y = $myPDO->query("
    SELECT * FROM `instructions` WHERE game_id = $game_id;
");

$games = $x->fetchAll();
$igra = $games[0];
$instructions = $y->fetchAll();
$title = $igra['name'];
include 'head.php';
?>

<body class="sp">
    <?php include 'header.php';?>
    <div class="background_sp"></div>
    
    <div class="nav-btn-right col-xs-12 hidden-lg hidden-md text-center btn_sp">
        <a href="https://www.brainster.io/business" class="btn1-nav btn btn-default">Обуки за компании</a>
        <a href="https://www.brainster.io/business" class="btn2-nav btn btn-default">Вработи наши студенти</a>
    </div>
    <div class="container col-md-10 col-md-offset-1 game_sp">
        <h2 class="game_name_sp"><?php echo $igra['name']; ?></h2>
        <div class="col-md-12 col-xs-12 row1_sp">
            <div class="panel-body-4 panel-body-sp col-sm-3 col-xs-12">
                <i class="far fa-clock col-xs-1 fa_sp"></i>
                <h5 class="time time_sp col-xs-10">Time Frame<span><br><?php echo $igra['time_frame']; ?></span> </h5>
            </div>
            <div class="panel-body-4 panel-body-sp col-sm-3 col-xs-12">
                <i class="fas fa-user-friends col-xs-1 fa_sp"></i>
                <h5 class="time time_sp col-xs-10">Group Size<span><br><?php echo $igra['group_size']; ?></span> </h5>
            </div>
            <div class="panel-body-4 panel-body-sp col-sm-3 col-xs-12">
                <i class="fas fa-landmark col-xs-1 fa_sp"></i>
                <h5 class="time time_sp col-xs-10">Facilitation Level<span><br><?php echo $igra['facilitation_level']; ?></span> </h5>
            </div>
            <div class="panel-body-4 panel-body-sp col-sm-3 col-xs-12">
                <i class="fas fa-paint-roller col-xs-1 fa_sp"></i>
                <h5 class="time time_sp col-xs-10">Materials<span><br><?php echo $igra['materials']; ?></span> </h5>
            </div>
        </div>
        <h2 class="desc_sp"><?php echo $igra['description']; ?></h2>
        <?php foreach ($instructions as $key => $val) { ?>
            <hr><h2 class="step_sp">Чекор <?php echo $val['step_number']; ?></h2>
            <h2 class="desc_sp"><?php echo $val['description']; ?></h2>
        <?php } ?>
        <div class="bottom-padding"></div>
    </div>

    <div class="background-footer-sp"></div>
    <?php include "footer.php";?>
    <?php include 'scripts.php';?>
</body>
</html>